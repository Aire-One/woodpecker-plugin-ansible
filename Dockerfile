FROM golang:1.22 as builder

COPY . /src
WORKDIR /src

RUN CGO_ENABLED=0 GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -ldflags '-s -w -extldflags "-static"' -v -a -tags netgo -o plugin-ansible ./cmd/ansible

FROM alpine:3.19

# renovate: datasource=github-releases depName=ansible/ansible extractVersion=^v(?<version>.*)$
ARG ANSIBLE_CORE_VERSION=2.16.5

COPY --from=builder /src/plugin-ansible /bin/plugin-ansible

RUN apk add -q --no-cache ca-certificates yaml-dev
RUN apk add -q --no-cache bash git curl rsync openssh-client sshpass py3-pip py3-requests py3-paramiko python3-dev libffi-dev libressl-dev libressl build-base && \
	pip3 install --no-cache-dir -U pip --break-system-packages && \
	pip3 install --no-cache-dir ansible-core==${ANSIBLE_CORE_VERSION} boto3 --break-system-packages && \
	apk del --no-cache python3-dev libffi-dev libressl-dev build-base

ENTRYPOINT ["/bin/plugin-ansible"]
