# plugin-ansible

<br/>
<p align="center">
<a href="https://ci.codeberg.org/repos/3265" target="_blank">
  <img src="https://ci.codeberg.org/api/badges/3265/status.svg" alt="status-badge" />
</a>
<a href="https://codeberg.org/woodpecker-plugins/ansible/releases" title="Latest release">
  <img src="https://img.shields.io/gitea/v/release/woodpecker-plugins/ansible?gitea_url=https%3A%2F%2Fcodeberg.org
" alt="Latest release">
</a>
  <a href="https://matrix.to/#/#woodpecker:matrix.org" title="Join the Matrix space at https://matrix.to/#/#woodpecker:matrix.org">
    <img src="https://img.shields.io/matrix/woodpecker:matrix.org?label=matrix" alt="Matrix space">
  </a>
  <a href="https://hub.docker.com/r/woodpeckerci/plugin-ansible" title="Docker pulls">
    <img src="https://img.shields.io/docker/pulls/woodpeckerci/plugin-ansible" alt="Docker pulls">
  </a>
  <a href="https://opensource.org/licenses/Apache-2.0" title="License: Apache-2.0">
    <img src="https://img.shields.io/badge/License-Apache%202.0-blue.svg" alt="License: Apache-2.0">
  </a>
</p>
<br/>

Woodpecker CI plugin to execute Ansible playbooks.
This plugin is a fork of [drone-plugins/drone-ansible](https://github.com/drone-plugins/drone-ansible) with substantial modifications to the source code.

## Images

Images are available on [Dockerhub](https://hub.docker.com/r/woodpeckerci/plugin-ansible) and in the [Codeberg registry](https://codeberg.org/woodpecker-plugins/-/packages/container/ansible/latest).

## License

This project is licensed under the Apache-2.0 License - see the [LICENSE](https://codeberg.org/woodpecker-plugins/ansible/src/branch/main/LICENSE) file for details.

## Maintainers

This plugin is maintained by @pat-s.

## Installing required python module dependencies

Many ansible modules require additional python dependencies to work.
Because ansible is run inside an alpine-based container, these dependencies must be installed dynamically during playbook execution.

It is important to use `delegate_to: localhost` as otherwise the pip module will install the dependency on the remote host, which will not have an effect.

```yaml
- name: Install required pip dependencies
  delegate_to: localhost
  ansible.builtin.pip:
    name: <name>
    state: present
    extra_args: --break-system-packages
```

Without `--break-system-packages` alpine will complain aiming for plain pip3 packages being installed system-wide.
Alternatively, one can also use the apk/packages module if the required pip module is available as an `python3-<name>` package.
