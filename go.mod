module codeberg.org/woodpecker-plugins/ansible

go 1.21

require (
	codeberg.org/woodpecker-plugins/go-plugin v0.4.1
	github.com/pkg/errors v0.9.1
	github.com/spf13/afero v1.11.0
	github.com/urfave/cli/v2 v2.27.1
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/joho/godotenv v1.5.1 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/rs/zerolog v1.32.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	golang.org/x/net v0.22.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
