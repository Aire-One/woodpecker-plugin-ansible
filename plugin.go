package ansible

import (
	"context"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"github.com/spf13/afero"
	"github.com/urfave/cli/v2"
)

var ansibleFolder = "/etc/ansible"
var ansibleConfig = "/etc/ansible/ansible.cfg"

var ansibleContent = `
[defaults]
host_key_checking = False
`

func (p *Plugin) Execute(ctx context.Context) error {
	for len(p.Settings.Inventories.Value()) == 0 {
		return errors.New("you must provide an inventory")
	}

	if err := p.loadPlaybooks(); err != nil {
		return err
	}

	if err := p.ansibleConfig(afero.NewMemMapFs()); err != nil {
		return err
	}

	if p.Settings.VaultPassword != "" {
		if err := p.vaultPass(); err != nil {
			return err
		}

		defer os.Remove(p.Settings.VaultPasswordFile)
	}

	if p.Settings.PrivateKey != "" {
		if err := p.privateKey(); err != nil {
			return err
		}

		defer os.Remove(p.Settings.PrivateKeyFile)
	}

	commands := []*exec.Cmd{
		p.versionCommand(),
	}

	if p.Settings.Requirements != "" {
		commands = append(commands, p.requirementsCommand())
	}

	if p.Settings.Galaxy != "" {
		commands = append(commands, p.galaxyCommand())
	}

	for _, inventory := range p.Settings.Inventories.Value() {
		commands = append(commands, p.ansibleCommand(inventory))
	}

	for _, cmd := range commands {
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		cmd.Env = os.Environ()
		cmd.Env = append(cmd.Env, "ANSIBLE_FORCE_COLOR=1")

		trace(cmd)

		if err := cmd.Run(); err != nil {
			return err
		}
	}

	return nil
}

func (p *Plugin) ansibleConfig(fs afero.Fs) error {
	if err := fs.MkdirAll(ansibleFolder, os.ModePerm); err != nil {
		return errors.Wrap(err, "failed to create ansible directory")
	}

	if err := afero.WriteFile(fs, ansibleConfig, []byte(ansibleContent), 0600); err != nil {
		return errors.Wrap(err, "failed to create ansible config")
	}

	return nil
}

func (p *Plugin) privateKey() error {
	tmpfile, err := os.CreateTemp("", "privateKey")

	if err != nil {
		return errors.Wrap(err, "failed to create private key file")
	}

	if _, err := tmpfile.Write([]byte(p.Settings.PrivateKey)); err != nil {
		return errors.Wrap(err, "failed to write private key file")
	}

	if err := tmpfile.Close(); err != nil {
		return errors.Wrap(err, "failed to close private key file")
	}

	p.Settings.PrivateKeyFile = tmpfile.Name()
	return nil
}

func (p *Plugin) vaultPass() error {
	tmpfile, err := os.CreateTemp("", "vaultPass")

	if err != nil {
		return errors.Wrap(err, "failed to create vault password file")
	}

	if _, err := tmpfile.Write([]byte(p.Settings.VaultPassword)); err != nil {
		return errors.Wrap(err, "failed to write vault password file")
	}

	if err := tmpfile.Close(); err != nil {
		return errors.Wrap(err, "failed to close vault password file")
	}

	p.Settings.VaultPasswordFile = tmpfile.Name()
	return nil
}

func (p *Plugin) loadPlaybooks() error {
	for len(p.Settings.Playbooks.Value()) == 0 {
		return errors.New("you must provide a playbook")
	}

	playbooks := cli.StringSlice{}
	for _, p := range p.Settings.Playbooks.Value() {
		files, err := filepath.Glob(p)
		if err != nil {
			playbooks.Set(p)
			continue
		}

		for _, file := range files {
			playbooks.Set(file)
		}
	}

	if len(playbooks.Value()) == 0 {
		return errors.New("failed to find playbook files")
	}

	p.Settings.Playbooks = playbooks
	return nil
}

func (p *Plugin) versionCommand() *exec.Cmd {
	args := []string{
		"--version",
	}

	return exec.Command(
		"ansible",
		args...,
	)
}

func (p *Plugin) requirementsCommand() *exec.Cmd {
	args := []string{
		"install",
		"--upgrade",
		"--requirement",
		p.Settings.Requirements,
	}

	return exec.Command(
		"pip3",
		args...,
	)
}

func (p *Plugin) galaxyCommand() *exec.Cmd {
	args := []string{
		"install",
	}

	if p.Settings.GalaxyForce {
		args = append(args, "--force")
	}

	args = append(args,
		"--role-file",
		p.Settings.Galaxy,
	)

	if p.Settings.Verbose > 0 {
		args = append(args, fmt.Sprintf("-%s", strings.Repeat("v", p.Settings.Verbose)))
	}

	return exec.Command(
		"ansible-galaxy",
		args...,
	)
}

func (p *Plugin) ansibleCommand(inventory string) *exec.Cmd {
	args := []string{
		"--inventory",
		inventory,
	}

	if len(p.Settings.ModulePath.Value()) > 0 {
		args = append(args, "--module-path", strings.Join(p.Settings.ModulePath.Value(), ":"))
	}

	if p.Settings.VaultID != "" {
		args = append(args, "--vault-id", p.Settings.VaultID)
	}

	if p.Settings.VaultPasswordFile != "" {
		args = append(args, "--vault-password-file", p.Settings.VaultPasswordFile)
	}

	for _, v := range p.Settings.ExtraVars.Value() {
		args = append(args, "--extra-vars", v)
	}

	if p.Settings.ListHosts {
		args = append(args, "--list-hosts")
		args = append(args, p.Settings.Playbooks.Value()...)

		return exec.Command(
			"ansible-playbook",
			args...,
		)
	}

	if p.Settings.SyntaxCheck {
		args = append(args, "--syntax-check")
		args = append(args, p.Settings.Playbooks.Value()...)

		return exec.Command(
			"ansible-playbook",
			args...,
		)
	}

	if p.Settings.Check {
		args = append(args, "--check")
	}

	if p.Settings.Diff {
		args = append(args, "--diff")
	}

	if p.Settings.FlushCache {
		args = append(args, "--flush-cache")
	}

	if p.Settings.ForceHandlers {
		args = append(args, "--force-handlers")
	}

	if p.Settings.Forks != 5 {
		args = append(args, "--forks", strconv.Itoa(p.Settings.Forks))
	}

	if p.Settings.Limit != "" {
		args = append(args, "--limit", p.Settings.Limit)
	}

	if p.Settings.ListTags {
		args = append(args, "--list-tags")
	}

	if p.Settings.ListTasks {
		args = append(args, "--list-tasks")
	}

	if p.Settings.SkipTags != "" {
		args = append(args, "--skip-tags", p.Settings.SkipTags)
	}

	if p.Settings.StartAtTask != "" {
		args = append(args, "--start-at-task", p.Settings.StartAtTask)
	}

	if p.Settings.Tags != "" {
		args = append(args, "--tags", p.Settings.Tags)
	}

	if p.Settings.PrivateKeyFile != "" {
		args = append(args, "--private-key", p.Settings.PrivateKeyFile)
	}

	if p.Settings.User != "" {
		args = append(args, "--user", p.Settings.User)
	}

	if p.Settings.Connection != "" {
		args = append(args, "--connection", p.Settings.Connection)
	}

	if p.Settings.Timeout != 0 {
		args = append(args, "--timeout", strconv.Itoa(p.Settings.Timeout))
	}

	if p.Settings.SSHCommonArgs != "" {
		args = append(args, "--ssh-common-args", p.Settings.SSHCommonArgs)
	}

	if p.Settings.SFTPExtraArgs != "" {
		args = append(args, "--sftp-extra-args", p.Settings.SFTPExtraArgs)
	}

	if p.Settings.SCPExtraArgs != "" {
		args = append(args, "--scp-extra-args", p.Settings.SCPExtraArgs)
	}

	if p.Settings.SSHExtraArgs != "" {
		args = append(args, "--ssh-extra-args", p.Settings.SSHExtraArgs)
	}

	if p.Settings.Become {
		args = append(args, "--become")
	}

	if p.Settings.BecomeMethod != "" {
		args = append(args, "--become-method", p.Settings.BecomeMethod)
	}

	if p.Settings.BecomeUser != "" {
		args = append(args, "--become-user", p.Settings.BecomeUser)
	}

	if p.Settings.Verbose > 0 {
		args = append(args, fmt.Sprintf("-%s", strings.Repeat("v", p.Settings.Verbose)))
	}

	args = append(args, p.Settings.Playbooks.Value()...)

	return exec.Command(
		"ansible-playbook",
		args...,
	)
}

func trace(cmd *exec.Cmd) {
	fmt.Println("$", strings.Join(cmd.Args, " "))
}
