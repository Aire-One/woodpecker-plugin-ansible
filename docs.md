---
name: Ansible
icon: https://codeberg.org/woodpecker-plugins/ansible/media/branch/main/ansible.png
description: Plugin to execute Ansible playbooks
author: Woodpecker Authors
tags: [ansible, playbook, automation]
containerImage: woodpeckerci/plugin-ansible
containerImageUrl: https://hub.docker.com/r/woodpeckerci/plugin-ansible
url: https://codeberg.org/woodpecker-plugins/ansible
---

# Overview

Woodpecker CI plugin to execute Ansible playbooks.
This plugin is a fork of [drone-plugins/drone-ansible](https://github.com/drone-plugins/drone-ansible) with substantial modifications of the source code.

## Features

- Install required dependencies before the start of a playbook
- Execute Ansible playbooks

## Installing required python module dependencies

Many ansible modules require additional python dependencies to work.
Because ansible is run inside an alpine-based container, these dependencies must be installed dynamically during playbook execution.

It is important to use `delegate_to: localhost` as otherwise the pip module will install the dependency on the remote host, which will not have an effect.

```yaml
- name: Install required pip dependencies
  delegate_to: localhost
  ansible.builtin.pip:
    name: <name>
    state: present
    extra_args: --break-system-packages
```

Without `--break-system-packages` alpine will complain aiming for plain pip3 packages being installed system-wide.
Alternatively, one can also use the apk/packages module if the required pip module is available as an `python3-<name>` package

## Settings

| Settings Name     | Default | Description                                           |
| ----------------- | ------- | ----------------------------------------------------- |
| `become-method`   | _none_  | privilege escalation method to use                    |
| `become-user`     | _none_  | run operations as this user                           |
| `become`          | `false` | run operations with become                            |
| `check`           | `false` | run in "check mode"/dry-run, do not apply changes     |
| `connection`      | _none_  | connection type to use                                |
| `diff`            | `false` | show the differences (may print secrets!)             |
| `extra-vars`      | _none_  | set additional variables as key=value                 |
| `flush-cache`     | `false` | clear the fact cache for every host in inventory      |
| `force-handlers`  | _none_  | run handlers even if a task fails                     |
| `forks`           | `5`     | number of parallel processes to use                   |
| `galaxy-force`    | `true`  | force overwriting an existing role or collection      |
| `galaxy`          | _none_  | path to galaxy requirements file                      |
| `inventory`       | _none_  | specify inventory host path                           |
| `limit`           | _none_  | limit selected hosts to an additional pattern         |
| `list-hosts`      | `false` | outputs a list of matching hosts                      |
| `list-tags`       | `false` | list all available tags                               |
| `list-tasks`      | `false` | list all tasks that would be executed                 |
| `module-path`     | _none_  | prepend paths to module library                       |
| `playbook`        | _none_  | list of playbooks to apply                            |
| `private-key`     | _none_  | SSH private key to connect to host                    |
| `requirements`    | _none_  | path to python requirements file                      |
| `scp-extra-args`  | _none_  | specify extra arguments to pass to scp only           |
| `sftp-extra-args` | _none_  | specify extra arguments to pass to sftp only          |
| `skip-tags`       | _none_  | skip tasks and playbooks with a matching tag          |
| `ssh-common-args` | _none_  | specify common arguments to pass to sftp/scp/ssh      |
| `ssh-extra-args`  | _none_  | specify extra arguments to pass to ssh only           |
| `start-at-task`   | _none_  | start the playbook at the task matching this **name** |
| `syntax-check`    | `false` | perform a syntax check on the playbook                |
| `tags`            | _none_  | only run plays and tasks tagged with these values     |
| `timeout`         | _none_  | override the connection timeout in seconds            |
| `user`            | _none_  | connect as this user                                  |
| `vault-id`        | _none_  | the vault identity to used                            |
| `vault-password`  | _none_  | vault password                                        |
| `verbose`         | `0`     | level of verbosity, 0 up to 4                         |
