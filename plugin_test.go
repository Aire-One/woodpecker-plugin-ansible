package ansible

import (
	"context"
	"log"
	"os"
	"testing"

	"github.com/spf13/afero"
	"github.com/urfave/cli/v2"
)

func TestPlugin_Execute(t *testing.T) {
	t.Run("with empty settings", func(t *testing.T) {
		p := &Plugin{
			Settings: &Settings{
				Requirements: "",
				Galaxy:       "",
				Inventories:  *cli.NewStringSlice(),
			},
		}

		err := p.Execute(context.Background())
		if err == nil {
			t.Errorf("Expected error, got nil")
		}
	})

	tmpfile, err := os.CreateTemp("", "example")
	if err != nil {
		log.Fatal(err)
	}
	defer os.Remove(tmpfile.Name()) // clean up

	t.Run("with valid settings", func(t *testing.T) {
		fs := afero.NewOsFs()
		// Create a mock requirements.yml file
		reqFileContent := "collections:\n  - name: community.general\n    version: 8.1.0"
		reqFilePath := "/tmp/requirements.yml"
		if err := os.WriteFile(reqFilePath, []byte(reqFileContent), 0644); err != nil {
			t.Fatal(err)
		}
		playbookContent := "---\n- hosts: all\n  tasks:\n    - name: Dummy task\n      debug:\n        msg: \"This is a dummy task\""
		playbookPath := "/tmp/playbook.yml"
		if err := os.WriteFile(playbookPath, []byte(playbookContent), 0644); err != nil {
			t.Fatal(err)
		}

		p := &Plugin{
			Settings: &Settings{
				Galaxy:      reqFilePath,
				Inventories: *cli.NewStringSlice("inventory"),
				Playbooks:   *cli.NewStringSlice(playbookPath),
				SyntaxCheck: true,
				Check:       true,
			},
		}

		p.ansibleConfig(fs)

		err := p.Execute(context.Background())
		if err != nil {
			t.Errorf("Expected no error, got '%v'", err)
		}
	})
}
