package ansible

import (
	"codeberg.org/woodpecker-plugins/go-plugin"
	"github.com/urfave/cli/v2"
)

// New initializes a plugin from the given Settings, Pipeline, and Network.
func New(version string) *Plugin {
	p := &Plugin{
		Settings: &Settings{},
	}

	p.Plugin = plugin.New(plugin.Options{
		Name:        "plugin-ansible",
		Description: "ansible plugin",
		Version:     version,
		Flags:       p.Flags(),
		Execute:     p.Execute,
	})

	return p
}

type (
	Plugin struct {
		*plugin.Plugin
		Settings *Settings
	}
	Settings struct {
		Become            bool            // Run operations with become
		BecomeMethod      string          // Privilege escalation method to use
		BecomeUser        string          // Run operations as this user
		Check             bool            // Don't make any changes; instead, try to predict some of the changes that may occur
		Connection        string          // Connection type to use
		Diff              bool            // When changing (small) files and templates, show the differences in those files
		ExtraVars         cli.StringSlice // Set additional variables
		FlushCache        bool            // Clear the fact cache for every host in inventory
		ForceHandlers     bool            // Run handlers even if a task fails
		Forks             int             // Specify number of parallel processes to use (default=5)
		Galaxy            string          // Galaxy server URL
		GalaxyForce       bool            // Force overwriting an existing role
		Inventories       cli.StringSlice // List of inventory files
		Limit             string          // Limit hosts to run on
		ListHosts         bool            // Outputs a list of matching hosts; does not execute anything else
		ListTags          bool            // List all available tags
		ListTasks         bool            // List all tasks that would be executed
		ModulePath        cli.StringSlice // Specify path(s) to module library
		Playbooks         cli.StringSlice // List of playbook files
		PrivateKey        string          // Use this file to authenticate the connection
		PrivateKeyFile    string          // Private key file
		Requirements      string          // Path to Python requirements
		SCPExtraArgs      string          // Specify extra arguments to pass to scp only
		SFTPExtraArgs     string          // Specify extra arguments to pass to sftp only
		SkipTags          string          // Tags to skip
		SSHCommonArgs     string          // Specify common arguments to pass to sftp/scp/ssh
		SSHExtraArgs      string          // Specify extra arguments to pass to ssh only
		StartAtTask       string          // Start at a specific task
		SyntaxCheck       bool            // Perform a syntax check on the playbook, but do not execute it
		Tags              string          // Only run tasks tagged with these values
		Timeout           int             // Override the connection timeout in seconds
		User              string          // Connect as this user
		VaultID           string          // The vault identity to use
		VaultPassword     string          // Vault password
		VaultPasswordFile string          // Vault password file
		Verbose           int             // Increase verbosity level
	}
)

func (p *Plugin) Flags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:        "requirements",
			Usage:       "path to python requirements",
			EnvVars:     []string{"PLUGIN_REQUIREMENTS"},
			Destination: &p.Settings.Requirements,
		},
		&cli.StringFlag{
			Name:        "galaxy",
			Usage:       "path to galaxy requirements",
			EnvVars:     []string{"PLUGIN_GALAXY"},
			Destination: &p.Settings.Galaxy,
		},
		&cli.StringSliceFlag{
			Name:        "inventory",
			Usage:       "specify inventory host path",
			EnvVars:     []string{"PLUGIN_INVENTORY", "PLUGIN_INVENTORIES"},
			Destination: &p.Settings.Inventories,
		},
		&cli.StringSliceFlag{
			Name:        "playbook",
			Usage:       "list of playbooks to apply",
			EnvVars:     []string{"PLUGIN_PLAYBOOK", "PLUGIN_PLAYBOOKS"},
			Destination: &p.Settings.Playbooks,
		},
		&cli.StringFlag{
			Name:        "limit",
			Usage:       "further limit selected hosts to an additional pattern",
			EnvVars:     []string{"PLUGIN_LIMIT"},
			Destination: &p.Settings.Limit,
		},
		&cli.StringFlag{
			Name:        "skip_tags",
			Usage:       "only run plays and tasks whose tags do not match",
			EnvVars:     []string{"PLUGIN_SKIP_TAGS"},
			Destination: &p.Settings.SkipTags,
		},
		&cli.StringFlag{
			Name:        "start_at_task",
			Usage:       "start the playbook at the task matching this name",
			EnvVars:     []string{"PLUGIN_START_AT_TASK"},
			Destination: &p.Settings.StartAtTask,
		},
		&cli.StringFlag{
			Name:        "tags",
			Usage:       "only run plays and tasks tagged with these values",
			EnvVars:     []string{"PLUGIN_TAGS"},
			Destination: &p.Settings.Tags,
		},
		&cli.StringSliceFlag{
			Name:        "extra_vars",
			Usage:       "set additional variables as key=value",
			EnvVars:     []string{"PLUGIN_EXTRA_VARS", "ANSIBLE_EXTRA_VARS"},
			Destination: &p.Settings.ExtraVars,
		},
		&cli.StringSliceFlag{
			Name:        "module_path",
			Usage:       "prepend paths to module library",
			EnvVars:     []string{"PLUGIN_MODULE_PATH"},
			Destination: &p.Settings.ModulePath,
		},
		&cli.BoolFlag{
			Name:        "galaxy_force",
			Usage:       "force overwriting an existing role or collection",
			EnvVars:     []string{"PLUGIN_GALAXY_FORCE"},
			Destination: &p.Settings.GalaxyForce,
		},
		&cli.BoolFlag{
			Name:        "check",
			Usage:       "run a check, do not apply any changes",
			EnvVars:     []string{"PLUGIN_CHECK"},
			Destination: &p.Settings.Check,
		},
		&cli.BoolFlag{
			Name:        "diff",
			Usage:       "show the differences, may print secrets",
			EnvVars:     []string{"PLUGIN_DIFF"},
			Destination: &p.Settings.Diff,
		},
		&cli.BoolFlag{
			Name:        "flush_cache",
			Usage:       "clear the fact cache for every host in inventory",
			EnvVars:     []string{"PLUGIN_FLUSH_CACHE"},
			Destination: &p.Settings.FlushCache,
		},
		&cli.BoolFlag{
			Name:        "force_handlers",
			Usage:       "run handlers even if a task fails",
			EnvVars:     []string{"PLUGIN_FORCE_HANDLERS"},
			Destination: &p.Settings.ForceHandlers,
		},
		&cli.BoolFlag{
			Name:        "list_hosts",
			Usage:       "outputs a list of matching hosts",
			EnvVars:     []string{"PLUGIN_LIST_HOSTS"},
			Destination: &p.Settings.ListHosts,
		},
		&cli.BoolFlag{
			Name:        "list_tags",
			Usage:       "list all available tags",
			EnvVars:     []string{"PLUGIN_LIST_TAGS"},
			Destination: &p.Settings.ListTags,
		},
		&cli.BoolFlag{
			Name:        "list_tasks",
			Usage:       "list all tasks that would be executed",
			EnvVars:     []string{"PLUGIN_LIST_TASKS"},
			Destination: &p.Settings.ListTasks,
		},
		&cli.BoolFlag{
			Name:        "syntax_check",
			Usage:       "perform a syntax check on the playbook",
			EnvVars:     []string{"PLUGIN_SYNTAX_CHECK"},
			Destination: &p.Settings.SyntaxCheck,
		},
		&cli.IntFlag{
			Name:        "forks",
			Usage:       "specify number of parallel processes to use",
			EnvVars:     []string{"PLUGIN_FORKS"},
			Destination: &p.Settings.Forks,
			Value:       5,
		},
		&cli.StringFlag{
			Name:        "vault_id",
			Usage:       "the vault identity to use",
			EnvVars:     []string{"PLUGIN_VAULT_ID", "ANSIBLE_VAULT_ID"},
			Destination: &p.Settings.VaultID,
		},
		&cli.StringFlag{
			Name:        "vault_password",
			Usage:       "the vault password to use",
			EnvVars:     []string{"PLUGIN_VAULT_PASSWORD", "ANSIBLE_VAULT_PASSWORD"},
			Destination: &p.Settings.VaultPassword,
		},
		&cli.IntFlag{
			Name:        "verbose",
			Usage:       "level of verbosity, 0 up to 4",
			EnvVars:     []string{"PLUGIN_VERBOSE"},
			Destination: &p.Settings.Verbose,
		},
		&cli.StringFlag{
			Name:        "private_key",
			Usage:       "use this key to authenticate the connection",
			EnvVars:     []string{"PLUGIN_PRIVATE_KEY", "ANSIBLE_PRIVATE_KEY"},
			Destination: &p.Settings.PrivateKey,
		},
		&cli.StringFlag{
			Name:        "user",
			Usage:       "connect as this user",
			EnvVars:     []string{"PLUGIN_USER,ANSIBLE_USER"},
			Destination: &p.Settings.User,
		},
		&cli.StringFlag{
			Name:        "connection",
			Usage:       "connection type to use",
			EnvVars:     []string{"PLUGIN_CONNECTION"},
			Destination: &p.Settings.Connection,
		},
		&cli.IntFlag{
			Name:        "timeout",
			Usage:       "override the connection timeout in seconds",
			EnvVars:     []string{"PLUGIN_TIMEOUT"},
			Destination: &p.Settings.Timeout,
		},
		&cli.StringFlag{
			Name:        "ssh_common_args",
			Usage:       "specify common arguments to pass to sftp/scp/ssh",
			EnvVars:     []string{"PLUGIN_SSH_COMMON_ARGS"},
			Destination: &p.Settings.SSHCommonArgs,
		},
		&cli.StringFlag{
			Name:        "sftp_extra_args",
			Usage:       "specify extra arguments to pass to sftp only",
			EnvVars:     []string{"PLUGIN_SFTP_EXTRA_ARGS"},
			Destination: &p.Settings.SFTPExtraArgs,
		},
		&cli.StringFlag{
			Name:        "scp_extra_args",
			Usage:       "specify extra arguments to pass to scp only",
			EnvVars:     []string{"PLUGIN_SCP_EXTRA_ARGS"},
			Destination: &p.Settings.SCPExtraArgs,
		},
		&cli.StringFlag{
			Name:        "ssh_extra_args",
			Usage:       "specify extra arguments to pass to ssh only",
			EnvVars:     []string{"PLUGIN_SSH_EXTRA_ARGS"},
			Destination: &p.Settings.SSHExtraArgs,
		},
		&cli.BoolFlag{
			Name:        "become",
			Usage:       "run operations with become",
			EnvVars:     []string{"PLUGIN_BECOME"},
			Destination: &p.Settings.Become,
		},
		&cli.StringFlag{
			Name:        "become_method",
			Usage:       "privilege escalation method to use",
			EnvVars:     []string{"PLUGIN_BECOME_METHOD", "ANSIBLE_BECOME_METHOD"},
			Destination: &p.Settings.BecomeMethod,
		},
		&cli.StringFlag{
			Name:        "become_user",
			Usage:       "run operations as this user",
			EnvVars:     []string{"PLUGIN_BECOME_USER", "ANSIBLE_BECOME_USER"},
			Destination: &p.Settings.BecomeUser,
		},
	}
}
